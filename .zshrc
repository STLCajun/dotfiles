# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="cloud"

# Example aliases
alias zshconfig="subl ~/.zshrc"
alias ohmyzsh="subl ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
# HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git-extras git brew node npm osx sublime bower rvm git-flow sudo vagrant)

source $ZSH/oh-my-zsh.sh
unsetopt correct_all

# User configuration

# # Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#  export EDITOR='vim'
# else
#  export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

export PATH="$HOME/.composer/bin:$PATH"
export PATH="$HOME/.composer/vendor/bin:$PATH"

#MAMP MYSQL
export PATH="/Applications/MAMP/bin/php/php5.6.7/bin:$PATH"
export PATH=$PATH:/Applications/MAMP/Library/bin 

#ALIASES//////////

#----Git command-------------------------
alias gs="git status"
alias glog="git log --color --full-history --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias ga.="git add ."
alias grv="git remote -v"
alias gba="git branch -a"
alias gpo="git push origin"
alias gp="git smart-pull"
alias gcm="git commit -m"
alias gca="git commit -am"
alias gco="git checkout"
alias grao="git remote add origin"
#alias gf="git fetch"

alias php="/Applications/MAMP/bin/php/php5.6.7/bin/php"

alias gitlog='git log --graph --all --full-history --color --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s"'

#alias gf='git flow'
#alias dir='ls -la'

alias www='cd /var/www/domains'
alias dir='ls -la'
alias ..='cd ..'
alias desk='cd ~/desktop'
alias o='open .'

alias rebash='source ~/.zshrc'

alias cmds='npm run-script cmds'

alias isim='open /Applications/Xcode.app/Contents/Developer/Applications/iOS\ Simulator.app'

alias sshqa="ssh pcrw@pc-test01.managed.contegix.com" #199.119.124.118
alias sshprod="ssh pcrw@pc-prod01.managed.contegix.com" #199.119.124.117
alias sshprod02="ssh pcrw@pc-prod02.managed.contegix.com" #209.117.253.5
alias sshprod03="ssh pcrw@pc-prod03.managed.contegix.com" #209.117.253.5
alias sshprod04="ssh pcrw@pc-prod04.managed.contegix.com" #209.117.253.5
alias sshprod05="ssh pcrw@pc-prod05.managed.contegix.com" #209.117.253.5
alias sshprod06="ssh pcrw@pc-prod06.managed.contegix.com" #209.117.253.5
alias sshprod07="ssh pcrw@pc-prod07.managed.contegix.com" #209.117.253.5
alias sshprod08="ssh pcrw@pc-prod08.managed.contegix.com" #209.117.253.5
alias sshprod09="ssh pcrw@pc-prod09.managed.contegix.com" #209.117.253.5
alias sshprod10="ssh pcrw@pc-prod10.managed.contegix.com" #209.117.253.5

#///Set up a server from a folder
alias servesimple="python -m SimpleHTTPServer 8001"
alias serve="php -S localhost:8003"

alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'

alias lsa='ls -d .* --color=auto'
alias host='sudo st /private/etc/hosts'
alias flushdns='sudo discoveryutil udnsflushcaches'

# Vagrant Calls
alias v='vagrant'
alias vssh='cd ~/vagrant; vagrant ssh'
alias vm='ssh vagrant@127.0.0.1 -p 2222'

# MongoDB
alias mongostart="mongod --fork --config /usr/local/etc/mongod.conf"
export PATH=$HOME/.node/bin:$PATH

alias deploydat='git commit -am "updates" && git push heroku master'

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="/System/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
